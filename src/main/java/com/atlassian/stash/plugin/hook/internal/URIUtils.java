package com.atlassian.stash.plugin.hook.internal;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class URIUtils {

	private static Pattern USER_INFO_PASS_LOCATOR = Pattern.compile("(?<=:).*$");

	private URIUtils() {
	}

	public static String sanitize(URI uri) {
		if (StringUtils.isEmpty(uri.getUserInfo())) {
			return uri.toString();
		} else {
			return rewrite(uri, USER_INFO_PASS_LOCATOR.matcher(uri.getUserInfo()).replaceFirst("*****"));
		}
	}

	public static String rewrite(URI uri, String newUserInfo) {
		try {
			return new URI(uri.getScheme(), newUserInfo, uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment())
					.toString();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

}
