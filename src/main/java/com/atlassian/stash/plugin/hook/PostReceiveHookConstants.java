package com.atlassian.stash.plugin.hook;

/**
 * Constants used by plugin.
 */
public final class PostReceiveHookConstants {

	/**
	 * Key of repository hook.
	 */
	public static final String REPOSITORY_HOOK_KEY = "stash-web-post-receive-hooks-plugin-repository-hook-config";

	/**
	 * Core size of thread pool - number of concurrent hooks notifications.
	 */
	public static final String THREAD_POOL_CORE_SIZE_PROPERTY = "plugin.com.atlassian.stash.plugin.hook.threadPoolCoreSize";

	/**
	 * Maximal size of thread pool - number of concurrent hooks notifications.
	 */
	public static final String THREAD_POOL_MAX_SIZE_PROPERTY = "plugin.com.atlassian.stash.plugin.hook.threadPoolMaxSize";

	/**
	 * Size of awaiting payloads for processing.
	 */
	public static final String QUEUE_SIZE_PROPERTY = "plugin.com.atlassian.stash.plugin.hook.queueSize";

	/**
	 * Connection timeout for hook request in milliseconds.
	 */
	public static final String HOOK_CONNECTION_TIMEOUT = "plugin.com.atlassian.stash.plugin.hook.connectionTimeout";

	/**
	 * Limit of maximal count of changesets per one hook.
	 */
	public static final String HOOK_CHANGESETS_LIMIT = "plugin.com.atlassian.stash.plugin.hook.changesetsLimit";
	
	/**
	 * Limit of maximal count of changes per one changeset of hook.
	 */
	public static final String HOOK_CHANGES_LIMIT = "plugin.com.atlassian.stash.plugin.hook.changesLimit";

	/**
	 * URL to documentation related to this plugin.
	 */
	public static final String PLUGIN_DOCUMENTATION_URL = "https://confluence.atlassian.com/display/STASH/POST+service+webhook+for+Stash";

	/**
	 * Singleton - only static members.
	 */
	private PostReceiveHookConstants() {
	}

}
