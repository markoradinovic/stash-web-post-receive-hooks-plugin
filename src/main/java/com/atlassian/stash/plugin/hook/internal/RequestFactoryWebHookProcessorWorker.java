package com.atlassian.stash.plugin.hook.internal;

import java.net.URISyntaxException;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseTimeoutException;

/**
 * Makes request call for provided {@link WebHookRequest}.
 */
public class RequestFactoryWebHookProcessorWorker implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(RequestFactoryWebHookProcessorWorker.class);

	private final RequestFactory<Request<?, Response>> requestFactory;
	private final WebHookRequest webHookRequest;
	private final int connectionTimeout;
	private final ApplicationProperties appProperties;

	public RequestFactoryWebHookProcessorWorker(int connectionTimeout, RequestFactory<Request<?, Response>> requestFactory,
			WebHookRequest webHookRequest, ApplicationProperties appProperties) {

		this.requestFactory = requestFactory;
		this.webHookRequest = webHookRequest;

		this.connectionTimeout = connectionTimeout;
		this.appProperties = appProperties;
	}

	public WebHookRequest getWebHookRequest() {
		return webHookRequest;
	}

	@Override
	public void run() {
		try {
			Request<?, Response> request = requestFactory.createRequest(MethodType.POST, URIUtils.rewrite(webHookRequest.getHookUri(), null));
			request.setSoTimeout(connectionTimeout);
			request.setConnectionTimeout(connectionTimeout);
			request.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
			request.setHeader(HttpHeaders.USER_AGENT, appProperties.getDisplayName() + " " + appProperties.getVersion());
			request.setEntity(webHookRequest.getRestPostReceiveHook().get());

			addUserInfo(request);

			try {
				request.execute(new ResponseHandler<Response>() {

					@Override
					public void handle(Response response) throws ResponseException {
						if (!response.isSuccessful()) {
							log.warn("Invalid response from hook '{}', repository ID '{}', status: '{}' / '{}'",
									URIUtils.sanitize(webHookRequest.getHookUri()), webHookRequest.getRepository().getId(), response.getStatusCode(),
									response.getStatusText());

						}
					}

				});

			} catch (ResponseTimeoutException e) {
				String message = "Unable to do post to '{}' hook for repository ID: '{}', Connection timeout setting: {}";
				String sanitizedHookUri = URIUtils.sanitize(webHookRequest.getHookUri());
				if (log.isDebugEnabled()) {
					log.debug(message, sanitizedHookUri, webHookRequest.getRepository().getId(), connectionTimeout, e);
				} else {
					log.warn(message, sanitizedHookUri, webHookRequest.getRepository().getId(), connectionTimeout);
				}

			} catch (ResponseException e) {
				String message = "Unable to do post to '{}' hook for repository ID: '{}', reason: {}";
				String sanitizedHookUri = URIUtils.sanitize(webHookRequest.getHookUri());
				if (log.isDebugEnabled()) {
					log.debug(message, sanitizedHookUri, webHookRequest.getRepository().getId(), e.getMessage(), e);
				} else {
					log.warn(message, sanitizedHookUri, webHookRequest.getRepository().getId(), e.getMessage());
				}
			}

		} catch (Exception e) {
			String message = "Unable to do post to '{}' hook for repository ID: '{}'";
			String sanitizedHookUri = URIUtils.sanitize(webHookRequest.getHookUri());
			log.warn(message, sanitizedHookUri, webHookRequest.getRepository().getId(), e);
		}

	}

	private void addUserInfo(Request<?, Response> request) throws URISyntaxException {
		String userInfo = webHookRequest.getHookUri().getUserInfo();
		if (!StringUtils.isEmpty(userInfo)) {
			int firstColonPosition = userInfo.indexOf(':');
			if (firstColonPosition > 0) {
				request.addBasicAuthentication(userInfo.substring(0, firstColonPosition), userInfo.substring(firstColonPosition + 1));
			} else {
				log.warn("User info in hook URL has invalid format! Username:password - both of them are mandatory for user info! ");
			}
		}
	}

}
