package com.atlassian.stash.plugin.hook;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.plugin.hook.internal.RequestFactoryWebHookProcessor;
import com.atlassian.stash.plugin.hook.internal.WebHookRequest;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

/**
 * Makes post/receive post commit hooks.
 */
public class WebPostReceiveHook implements AsyncPostReceiveRepositoryHook {

	/**
	 * Holds key prefix for {@link Settings#getString(String)}, which holds post-commit hook.
	 */
	public static final String URL_SETTINGS_PREFIX = "hook-url-";

	private final RequestFactoryWebHookProcessor postReceiveHookProcessor;
	private final SecurityService securityService;
	private final HistoryService historyService;
	private final NavBuilder navBuilder;
	private final StashAuthenticationContext authenticationContext;

	private final int changesetsLimit;
	private final int changesLimit;

	public WebPostReceiveHook(RequestFactoryWebHookProcessor postReceiveHookProcessor,
			ApplicationPropertiesService applicationPropertiesService, SecurityService securityService, HistoryService historyService,
			NavBuilder navBuilder, StashAuthenticationContext authenticationContext) {

		this.postReceiveHookProcessor = postReceiveHookProcessor;

		this.changesetsLimit = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.HOOK_CHANGESETS_LIMIT, 500);
		this.changesLimit = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.HOOK_CHANGES_LIMIT, 100);

		this.securityService = securityService;
		this.historyService = historyService;
		this.navBuilder = navBuilder;
		this.authenticationContext = authenticationContext;
	}

	@Override
	public void postReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges) {
		checkNotNull(context);
		checkNotNull(refChanges);

		StashUser currentUser = authenticationContext.getCurrentUser();
		for (URI hook : resolveHooksURIs(context.getSettings())) {
			postReceiveHookProcessor.processAsync(new WebHookRequest(securityService, historyService, navBuilder, changesetsLimit,
					changesLimit, context.getRepository(), refChanges, hook, currentUser));
		}
	}

	private List<URI> resolveHooksURIs(final Settings settings) {
		List<URI> result = new LinkedList<URI>();

		for (String key : settings.asMap().keySet()) {
			if (key.startsWith(URL_SETTINGS_PREFIX)) {
				result.add(URI.create(settings.getString(key)));
			}
		}

		return result;
	}

}
