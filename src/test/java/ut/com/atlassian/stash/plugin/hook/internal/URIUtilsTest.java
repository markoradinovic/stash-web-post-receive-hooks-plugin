package ut.com.atlassian.stash.plugin.hook.internal;

import java.net.URI;

import org.junit.Assert;
import org.junit.Test;

import com.atlassian.stash.plugin.hook.internal.URIUtils;

public class URIUtilsTest {
	
	@Test
	public void testRewrite() throws Exception {
		URI uri = new URI("scheme", "user:pass", "host", -1, "/path", "query", "fragment");
		String newUserInfo = "newUser:newPass";
		String newUri = URIUtils.rewrite(uri, newUserInfo);
		Assert.assertEquals("scheme://" + newUserInfo + "@host/path?query#fragment", newUri);
	}

	@Test
	public void testSanitizeWithUserInfo() throws Exception {
		URI uri = new URI("scheme", "user:passPart1:passPart2", "host", -1, "/path", "query", "fragment");
		Assert.assertEquals("scheme://user:*****@host/path?query#fragment", URIUtils.sanitize(uri));
		
	}

	@Test
	public void testSanitizeWithoutUserInfo() throws Exception {
		URI uri = new URI("scheme", null, "host", -1, "/path", "query", "fragment");
		Assert.assertEquals("scheme://host/path?query#fragment", URIUtils.sanitize(uri));
	}
	
}
